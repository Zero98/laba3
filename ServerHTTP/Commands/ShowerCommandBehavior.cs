﻿using System.Collections.Generic;
using ServerHTTP.Model;

namespace ServerHTTP.Commands
{
    class ShowerCommandBehavior : ICommandBehavior
    {        
        public string ExecuteAction(ref List<Song> songs, TransferData transferData)
        {
            string songsList = ""; 
            foreach (var song in songs)
            {
                songsList += $"{song.Author} - {song.Name}\n";
            }

            return songsList.TrimEnd('\n');
        }
    }
}
