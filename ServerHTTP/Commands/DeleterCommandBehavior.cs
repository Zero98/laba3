﻿using System.Collections.Generic;
using ServerHTTP.Model;

namespace ServerHTTP.Commands
{
    class DeleterCommandBehavior : ICommandBehavior
    {
        public string ExecuteAction(ref List<Song> songs, TransferData transferData)
        {
            string queryString = transferData.Song.Author + '-' + transferData.Song.Name;
            string callbackString = "";

            if (songs.RemoveAll(n => queryString.Contains(n.Author + " - " + n.Name)) > 0)
            {
                callbackString = $"Track \"{queryString}\" deleted";
            }
            else
            {
                callbackString = $"The collection doesn't contain this {queryString}";
            }

            return callbackString;
        }
    }
}
