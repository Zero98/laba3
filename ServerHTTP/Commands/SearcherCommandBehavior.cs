﻿using System.Collections.Generic;
using System.Threading;
using ServerHTTP.Model;

namespace ServerHTTP.Commands
{
    class SearcherCommandBehavior : ICommandBehavior
    {
        public string ExecuteAction(ref List<Song> songs, TransferData transferData)
        {
            string queryString = transferData.Song.Name;

            List<Song> querySongs = songs.FindAll(n => (n.Author + " - " + n.Name).Contains(queryString));

            ShowerCommandBehavior shower = new ShowerCommandBehavior();
            
            return shower.ExecuteAction(ref querySongs, transferData);
        }
    }
}
