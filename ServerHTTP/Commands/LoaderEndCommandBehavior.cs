﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using ServerHTTP.Model;

namespace ServerHTTP.Commands
{
    class LoaderEndCommandBehavior : ICommandBehavior
    {   
        struct SongField
        {
            public string SongReference;
            public string SongAuthor;
            public string SongName;
            public string SongLength;
            public int? SongID;
            public bool Empty;
        }

        /// <summary>
        /// Loads a playlist from a pls file
        /// </summary>
        public string ExecuteAction(ref List<Song> songs, TransferData transferData)
        {
            List<Song> newSongs = null; 
            string[] filesname = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.pls");

            if(filesname.Contains(transferData.FileName))
            {
                newSongs = LoadFile(transferData.FileName);
            }

            return GetLoadResault(newSongs, ref songs);
        }

        private string GetLoadResault(List<Song> newSongs, ref List<Song> songs)
        {
            string resaultString = "";
            if (!(newSongs == null))
            {
                resaultString = "Successful!";
                songs = newSongs;
            }
            else
                resaultString = "Unsuccessful!";

            return resaultString;
        }
        
        private List<Song> LoadFile(string filename)
        {
            List<Song> songs = new List<Song>();

            using (StreamReader reader = new StreamReader(filename))
            {
                int countOfEntries;

                try
                {
                    if (reader.ReadLine() == "[playlist]" &&
                            Int32.TryParse(
                            reader.ReadLine().Replace("NumberOfEntries=", ""),
                            out countOfEntries))
                    {
                        SongField helpSongField = new SongField() { Empty = true };
                        for (int itter = 0; itter < countOfEntries; itter++)
                        {
                            SongField song = helpSongField;

                            song.SongID = itter;

                            while (!IsNewSong(reader.ReadLine(), ref song, ref helpSongField)) ;

                            AddNewSong(song, songs);
                        }
                    }
                    else
                    {
                        Console.WriteLine("It was entered incorrect value");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }
            return songs;
        }

        private void AddNewSong(SongField song, List<Song> songs)
        {
            if (!song.Empty)
            {
                songs.Add(new Song(
                    song.SongAuthor,
                    song.SongName,
                    song.SongReference,
                    song.SongLength != null ? int.Parse(song.SongLength) : -1)
                    );
            }
        }

        private bool IsNewSong(string line, ref SongField song, ref SongField helpSongFiled)
        {
            if (!String.IsNullOrEmpty(line))
            {
                if (line.Count(n => n == '=') == 1)
                {
                    var lines = line.Split('=');

                    var songID = int.Parse(
                        new string(
                                lines[0].ToCharArray().Where(
                                n => char.IsDigit(n)).ToArray()));
                    var songField = new string(
                        lines[0].ToCharArray().Where(
                            n => !char.IsDigit(n)).ToArray());

                    return song.SongID == songID || song.SongID == null
                        ? FillField(
                            songField,
                            lines[1], ref song)
                        : FillField(
                            songField,
                            lines[1],
                            ref helpSongFiled, true);
                }
                song.Empty = true;
            }
            return true;
        }

        private bool FillField(string stringFild, string valueField, ref SongField song, bool isNew = false)
        {
            switch (stringFild)
            {
                case "File":
                    {
                        song.SongReference = valueField;
                        break;
                    }
                case "Title":
                    {
                        Regex regex = new Regex(@"\s[-]\s");

                        var splittedValueField = regex.Split(valueField);
                        song.SongAuthor = splittedValueField[0];
                        song.SongName = splittedValueField[1];

                        break;
                    }
                case "Length":
                    {
                        song.SongLength = valueField;
                        break;
                    }
                default:
                    {
                        throw new Exception("Wrong field in the file!");
                    }
            }
            song.Empty = false;
            return isNew;
        }
    }
}
