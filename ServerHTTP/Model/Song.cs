﻿namespace ServerHTTP.Model
{
    public class Song
    {
        public string Author { get; set; }

        public string Name { get; set; }

        public string Reference { get; set; }

        public int Length { get; set; }

        public Song(string author, string name, string reference, int length = -1)
        {
            Author = author;
            Name = name;
            Reference = reference;
            Length = -1;
        }

        public Song() { }

        public override string ToString()
        {
            return $"{Author} - {Name}";
        }

        public bool IsCorrectSong()
        {
            return Name != default(string);
        }
    }
}
