﻿using System.Collections.Generic;

namespace ServerHTTP.Model
{
    class Client
    {
        private List<Song> playList;
        
        public Client()
        {
            playList = new List<Song>();
        }

        internal string ExecuteCommand(TransferData transferData)
        {
            CommandHandler commandHandler = new CommandHandler();
            return commandHandler.Handle(transferData,ref playList);
        }
    }
}
