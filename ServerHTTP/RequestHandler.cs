﻿using System;
using System.Text;
using System.Net.Sockets;
using System.Text.RegularExpressions;
using ServerHTTP.Model;

namespace ServerHTTP
{
    public class RequestHandler
    {
        public TransferData GetRequest(string request)
        {
            if (String.IsNullOrEmpty(request))
                return null;
            Regex regex = new Regex(@"..xml.*", RegexOptions.Singleline);
            string xmlString = regex.Match(request).Value;
            
            return TransferData.ConvertToTransferData(xmlString);
        }

        public void SendResponse(string data, NetworkStream stream)
        {
            string content = $"{HttpServer.VERSION} 200 OK\n" +
                        $"Server: {HttpServer.NAME}\n" +
                        $"Accept-Ranges: bytes\n" +
                        $"Content-Lenght: {data.Length}\n\n{data}";

            stream.Write(Encoding.UTF8.GetBytes(content), 0, content.Length);
        }
    }
}
