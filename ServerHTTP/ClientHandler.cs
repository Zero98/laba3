﻿using System;
using System.Collections.Generic;
using System.Threading;
using ServerHTTP.Model;

namespace ServerHTTP
{
    public sealed class ClientHandler
    {
        private static readonly Object s_lock = new Object();

        private static ClientHandler clientHandler = null;

        private static Dictionary<int, Client> clients;

        public static ClientHandler Instance
        {
            get
            {
                if (clientHandler != null)
                    return clientHandler;

                Monitor.Enter(s_lock);
                try
                {
                    ClientHandler temp = new ClientHandler();
                    Interlocked.Exchange(ref clientHandler, temp);
                }
                catch(Exception ex)
                {
                    HttpServer.WriteLine(ex.Message);
                }
                finally
                {
                    Monitor.Exit(s_lock);
                }
                return clientHandler;
            }
        }

        private ClientHandler()
        {
            clients = new Dictionary<int, Client>();
        }
        
        public string HandleClient(TransferData transferData)
        {
            Client client = GetUser(transferData.UserID);

            return client.ExecuteCommand(transferData);
        }

        private Client GetUser(int userID)
        {
            if (clients.ContainsKey(userID))
                return clients[userID];

            clients.Add(userID, new Client());
            return clients[userID];
        }
    }
}
