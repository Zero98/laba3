﻿namespace ClientMusicCatalog.Commands
{
    class ExitCommandBehavior : ICommandBehavior
    {
        /// <summary>
        /// Called before program terminates
        /// </summary>
        public void ExecuteAction()
        {
            ClientMenu.WriteLine("Bye!");
        }
    }
}
