﻿namespace ClientMusicCatalog.Commands
{
    /// <summary>
    /// Command Interface
    /// </summary>
    interface ICommandBehavior
    {
        /// <summary>
        /// You need to implement the command
        /// </summary>
        void ExecuteAction();
    }
}
