﻿using System;
using ClientMusicCatalog.Model;

namespace ClientMusicCatalog
{
    class Program
    {
            static void Main(string[] args)
            {
                int serverPort;
                
                try
                {
                    if (int.TryParse(args[1], out serverPort))
                    {
                        Connection.InitServer(args[0],serverPort);

                        ClientMenu clientMenu = new ClientMenu();
                        clientMenu.RunClient();
                    }
                    else
                    {
                        Console.WriteLine("Please, enter port of server and host of server");
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

                Console.ReadKey();
        }
    }
}
